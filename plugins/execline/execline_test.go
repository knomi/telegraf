package execline

import (
	"fmt"
	"github.com/influxdb/telegraf/testutil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
//	"math"
	"testing"
	"time"
)

// Midnight 9/22/2015
const baseTimeSeconds = 1442905200

const validLines = `#
requests,route=a,method=GET username="woaza",uid="1",status=200i,ns=7326389i 1447840027722000000
requests,route=b/c,method=POST username="nobody",uid="2",status=201i,ns=16039829i 1447840028745000000
requests,route=d/e,method=GET username="user",uid="3",status=200i,ns=2567319667i 1447840406564000000
`

const malformedLines = `requests
`

type runnerMock struct {
	out []byte
	err error
}

type clockMock struct {
	now time.Time
}

func newRunnerMock(out []byte, err error) Runner {
	return &runnerMock{
		out: out,
		err: err,
	}
}

func (r runnerMock) Run(command *Command) ([]byte, error) {
	if r.err != nil {
		return nil, r.err
	}
	return r.out, nil
}

func newClockMock(now time.Time) Clock {
	return &clockMock{now: now}
}

func (c clockMock) Now() time.Time {
	return c.now
}

func TestExecLine(t *testing.T) {
	runner := newRunnerMock([]byte(validLines), nil)
	clock := newClockMock(time.Unix(baseTimeSeconds+20, 0))
	command := Command{
		Command:   "testcommand arg1",
		Name:      "mycollector",
		Interval:  10,
		lastRunAt: time.Unix(baseTimeSeconds, 0),
	}

	e := &ExecLine{
		runner:   runner,
		clock:    clock,
		Commands: []*Command{&command},
	}

	var acc testutil.Accumulator
	initialPoints := len(acc.Points)
	err := e.Gather(&acc)
	deltaPoints := len(acc.Points) - initialPoints
	require.NoError(t, err)

	acc.CheckTaggedFieldsValue("request",
		map[string]interface{}{
			"username": "woaza",
			"uid": "1",
			"status": 200,
			"ns": 7326389,
		},
		map[string]string{
			"route": "a",
			"method": "GET",
		});
	acc.CheckTaggedFieldsValue("request",
		map[string]interface{}{
			"username": "nobody",
			"uid": "2",
			"status": 201,
			"ns": 16039829,
		},
		map[string]string{
			"route": "b/c",
			"method": "POST",
		});
	acc.CheckTaggedFieldsValue("request",
		map[string]interface{}{
			"username": "user",
			"uid": "3",
			"status": 200,
			"ns": 2567319667,
		},
		map[string]string{
			"route": "d/e",
			"method": "GET",
		});
	assert.Equal(t, 3, deltaPoints, "expected 3 measurements")
}

func TestExecLineMalformed(t *testing.T) {
	runner := newRunnerMock([]byte(malformedLines), nil)
	clock := newClockMock(time.Unix(baseTimeSeconds+20, 0))
	command := Command{
		Command:   "badcommand arg1",
		Name:      "mycollector",
		Interval:  10,
		lastRunAt: time.Unix(baseTimeSeconds, 0),
	}

	e := &ExecLine{
		runner:   runner,
		clock:    clock,
		Commands: []*Command{&command},
	}

	var acc testutil.Accumulator
	initialPoints := len(acc.Points)
	err := e.Gather(&acc)
	deltaPoints := len(acc.Points) - initialPoints
	require.Error(t, err)

	assert.Equal(t, deltaPoints, 0, "No new points should have been added")
}

func TestCommandError(t *testing.T) {
	runner := newRunnerMock(nil, fmt.Errorf("exit status code 1"))
	clock := newClockMock(time.Unix(baseTimeSeconds+20, 0))
	command := Command{
		Command:   "badcommand",
		Name:      "mycollector",
		Interval:  10,
		lastRunAt: time.Unix(baseTimeSeconds, 0),
	}

	e := &ExecLine{
		runner:   runner,
		clock:    clock,
		Commands: []*Command{&command},
	}

	var acc testutil.Accumulator
	initialPoints := len(acc.Points)
	err := e.Gather(&acc)
	deltaPoints := len(acc.Points) - initialPoints
	require.Error(t, err)

	assert.Equal(t, deltaPoints, 0, "No new points should have been added")
}

func TestExecLineNotEnoughTime(t *testing.T) {
	runner := newRunnerMock([]byte(validLines), nil)
	clock := newClockMock(time.Unix(baseTimeSeconds+5, 0))
	command := Command{
		Command:   "testcommand arg1",
		Name:      "mycollector",
		Interval:  10,
		lastRunAt: time.Unix(baseTimeSeconds, 0),
	}

	e := &ExecLine{
		runner:   runner,
		clock:    clock,
		Commands: []*Command{&command},
	}

	var acc testutil.Accumulator
	initialPoints := len(acc.Points)
	err := e.Gather(&acc)
	deltaPoints := len(acc.Points) - initialPoints
	require.NoError(t, err)

	assert.Equal(t, deltaPoints, 0, "No new points should have been added")
}
