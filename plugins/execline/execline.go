package execline

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/gonuts/go-shellquote"
	"github.com/influxdb/telegraf/plugins"
	"github.com/influxdb/telegraf"
	"math"
	"os/exec"
	"strings"
	"sync"
	"time"
)

const sampleConfig = `
  # specify commands via an array of tables
  [[execline.commands]]
  # the command to run
  command = "/usr/bin/mycollector --foo=bar"

  # name of the command (used as a prefix for measurements)
  name = "mycollector"

  # Only run this command if it has been at least this many
  # seconds since it last ran
  interval = 10
`

type ExecLine struct {
	Commands []*Command
	runner   Runner
	clock    Clock
}

type Command struct {
	Command   string
	Name      string
	Interval  int
	lastRunAt time.Time
}

type Runner interface {
	Run(*Command) ([]byte, error)
}

type Clock interface {
	Now() time.Time
}

type CommandRunner struct{}

type RealClock struct{}

func (c CommandRunner) Run(command *Command) ([]byte, error) {
	command.lastRunAt = time.Now()
	split_cmd, err := shellquote.Split(command.Command)
	if err != nil || len(split_cmd) == 0 {
		return nil, fmt.Errorf("execline: unable to parse command, %s", err)
	}

	cmd := exec.Command(split_cmd[0], split_cmd[1:]...)
	var out bytes.Buffer
	cmd.Stdout = &out

	if err := cmd.Run(); err != nil {
		return nil, fmt.Errorf("execline: %s for command '%s'", err, command.Command)
	}

	return out.Bytes(), nil
}

func (c RealClock) Now() time.Time {
	return time.Now()
}

func NewExecLine() *ExecLine {
	return &ExecLine{runner: CommandRunner{}, clock: RealClock{}}
}

func (e *ExecLine) SampleConfig() string {
	return sampleConfig
}

func (e *ExecLine) Description() string {
	return "Read flattened metrics from one or more commands that output JSON to stdout"
}

func (e *ExecLine) Gather(acc plugins.Accumulator) error {
	acc.SetPrefix("")

	var wg sync.WaitGroup

	errorChannel := make(chan error, len(e.Commands))

	for _, c := range e.Commands {
		wg.Add(1)
		go func(c *Command, acc plugins.Accumulator) {
			defer wg.Done()
			err := e.gatherCommand(c, acc)
			if err != nil {
				errorChannel <- err
			}
		}(c, acc)
	}

	wg.Wait()
	close(errorChannel)

	// Get all errors and return them as one giant error
	errorStrings := []string{}
	for err := range errorChannel {
		errorStrings = append(errorStrings, err.Error())
	}

	if len(errorStrings) == 0 {
		return nil
	}
	return errors.New(strings.Join(errorStrings, "\n"))
}

func (e *ExecLine) gatherCommand(c *Command, acc plugins.Accumulator) error {
	secondsSinceLastRun := 0.0

	if c.lastRunAt.Unix() == 0 { // means time is uninitialized
		secondsSinceLastRun = math.Inf(1)
	} else {
		secondsSinceLastRun = (e.clock.Now().Sub(c.lastRunAt)).Seconds()
	}

	var firstError error
	if secondsSinceLastRun >= float64(c.Interval) {
		out, err := e.runner.Run(c)
		if err != nil {
			return err
		}

		lines := bytes.Split(out, []byte("\n"))
		for _, line := range lines {
			points, err := telegraf.ParsePoints(line)
			if err != nil && firstError == nil{
				firstError = err
			}
			for _, point := range points {
				acc.AddFields(point.Name(), point.Fields(), point.Tags(), point.Time())
			}
		}
	}
	return firstError
}

func processResponse(acc plugins.Accumulator, prefix string, tags map[string]string, v interface{}) {
	switch t := v.(type) {
	case map[string]interface{}:
		for k, v := range t {
			processResponse(acc, prefix+"_"+k, tags, v)
		}
	case string:
		// XXX handle error
		points, _ := telegraf.ParsePointsString(v.(string))
		for _, point := range points {
			acc.AddFields(point.Name(), point.Fields(), point.Tags(), point.Time())
		}
	}
}

func init() {
	plugins.Add("execline", func() plugins.Plugin {
		return NewExecLine()
	})
}
