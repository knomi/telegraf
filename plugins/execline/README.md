# ExecLine Plugin

The execline plugin can execute arbitrary commands which output line protocol points.

For example, if you have a log file with line protocol data:

```
[[exec.commands]]
command = "logtail2 line_protocol.log"
name = "mycollector"
interval = 10
```

The name is used as a prefix for the measurements.

The interval is used to determine how often a particular command should be run. Each
time the exec plugin runs, it will only run a particular command if it has been at least
`interval` seconds since the exec plugin last ran the command.
